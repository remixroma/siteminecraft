from django.shortcuts import render
from django.http import HttpResponse
from mcipc.query import Client
from django.shortcuts import redirect

def get_new_url(request):
    # Specify the port number, you could get this dynamically
    # through a config file or something if you wish
    new_port = '5000'

    # `request.get_host()` gives us {hostname}:{port}
    # we split this by colon to just obtain the hostname
    hostname = request.get_host().split(':')[0]
    # Construct the new url to redirect to
    url = 'http://' + hostname + ':' + new_port + '/'
    return redirect(url)

def index2(request):
    hostname = request.get_host().split(':')[0]
    if hostname != 'borovik.fun':
        return redirect('http://borovik.fun')


def index(request):
    try:
        with Client('95.217.211.124', 25565) as client:
            full_stats = client.full_stats
        context = {'stats':full_stats, 'is_oneline':True}
    except:
        context = {'stats':{},'is_oneline':False}
    return render(request, 'hat.html', context)



